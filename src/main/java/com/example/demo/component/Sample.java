package com.example.demo.component;

import com.baidu.aip.ocr.AipOcr;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 百度云token
 */
@Configuration
public class Sample {

    //设置APPID/AK/SK
    @Value("${api.baidu.com.appid}")
    private String APP_ID;
    @Value("${api.baidu.com.appkey}")
    private String API_KEY;
    @Value("${api.baidu.com.appsecret}")
    private String SECRET_KEY;

    @Bean
    public AipOcr createAipOcr(){

        AipOcr client = new AipOcr(APP_ID, API_KEY, SECRET_KEY);

        //建立连接的超时时间（单位：毫秒）
        client.setConnectionTimeoutInMillis(2000);
        //通过打开的连接传输数据的超时时间（单位：毫秒）
        client.setSocketTimeoutInMillis(60000);

        return client;
    }



}
